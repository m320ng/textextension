import gradio as gr
# from modules.ui_components import FormRow, FormGroup, ToolButton, FormHTML
# from modules.ui import  create_sampler_and_steps_selection
# from modules import ui_extra_networks, devices, shared, scripts, script_callbacks, sd_hijack_unet, sd_hijack_utils

def process_video(video,input_folder,input_fps,is_rasize,resize_width,resize_height,target_fps,sampler,scheduler,temporalnet_model,prompt,neg_prompt,denoise,output_images_folder,output_video_folder):

    return "processed"


def create_face_temporal_net():
    cn_models = ["None", "Test"]

    with gr.Column(visible=True, elem_id="face_temporal_net") as main_panel:
        dummy_component = gr.Label(visible=False)
        with gr.Row():
            with gr.Tab(label="Input Video"):
                video = gr.Video(label="Input Video", elem_id="input_video")
            with gr.Tab(label="Input Frame Images"):
                input_folder = gr.Textbox(label="Input Frame Images Folder",placeholder="Input Frame Images Folder")
                input_fps = gr.Number(value=30, precision=1, label="Input FPS", interactive=True)
        with gr.Row():
            is_rasize = gr.Checkbox(label="Resize", value=False)
            resize_width = gr.Number(value=0,label="Resize Width", precision=1, interactive=True)
            resize_height = gr.Number(value=0,label="Resize Height", precision=1, interactive=True)
        with gr.Row():
            target_fps = gr.Number(value=30, precision=1, label="Target FPS", interactive=True)
        with gr.Row():
            sampler = gr.Dropdown(
                label="Sampler",
                choices=webui_info.sampler_names,
                value=webui_info.sampler_names[0],
                visible=True,
            )
            scheduler = gr.Dropdown(
                label="Scheduler",
                choices=webui_info.scheduler_names,
                value=webui_info.scheduler_names[0],
                visible=len(webui_info.scheduler_names) > 1,
            )            
        with gr.Row():
            temporalnet_model = gr.Dropdown(
                label="TemporalNet Models",
                choices=cn_models,
                value=cn_models[0],
                visible=True,
            )
        with gr.Row():
            prompt = gr.Textbox(
                label="Prompt",
                lines=3,
                placeholder="Prompt",
            )
            neg_prompt = gr.Textbox(
                label="Negative Prompt",
                lines=3,
                placeholder="egative Prompt",
            )
        with gr.Row():
            denoise = gr.Slider(
                label="Denoise",
                minimum=0.0,
                maximum=1.0,
                step=0.01,
                value=0.40,
                visible=True,
                interactive=True,
            )
        with gr.Row():
            output_images_folder = gr.Textbox(label="Output Images Folder", placeholder="This is ignored if neither batch run or ebsynth are checked")
        with gr.Row():
            output_video_folder = gr.Textbox(label="Output Video Folder", placeholder="This is ignored if neither batch run or ebsynth are checked")

        with gr.Row():
            runbutton = gr.Button("Run") 
                                    
    runbutton.click(process_video, [video,input_folder,input_fps,is_rasize,resize_width,resize_height,target_fps,sampler,scheduler,temporalnet_model,prompt,neg_prompt,denoise,output_images_folder,output_video_folder])

def controlnet():
    cn_models = ["None", "Test"]

    with gr.Row(variant="panel"):
        with gr.Column(variant="compact"):
            controlnet_model = gr.Dropdown(
                label="ControlNet model",
                choices=cn_models,
                value="None",
                visible=True,
                type="value",
                interactive=True,
            )

            controlnet_module = gr.Dropdown(
                label="ControlNet module",
                choices=["None"],
                value="None",
                visible=False,
                type="value",
                interactive=True,
            )

            controlnet_weight = gr.Slider(
                label="ControlNet weight",
                minimum=0.0,
                maximum=1.0,
                step=0.01,
                value=1.0,
                visible=True,
                interactive=True,
            )

        with gr.Column(variant="compact"):
            controlnet_guidance_start = gr.Slider(
                label="ControlNet guidance start",
                minimum=0.0,
                maximum=1.0,
                step=0.01,
                value=0.0,
                visible=True,
                interactive=True,
            )

            controlnet_guidance_end = gr.Slider(
                label="ControlNet guidance end",
                minimum=0.0,
                maximum=1.0,
                step=0.01,
                value=1.0,
                visible=True,
                interactive=True,
            )

def on_ui_tabs():   
    with gr.Blocks(analytics_enabled=False) as face_temporal_net:
        create_face_temporal_net()

        return (face_temporal_net, "FaceTemporalNet", "FaceTemporalNet")


def greet(name, intensity):
    return "Hello, " + name + "!" * int(intensity)

demo = gr.Interface(
    fn=greet,
    inputs=["text", "slider"],
    outputs=["text"],
)


if __name__=="__main__":
    (demo, _, _) = on_ui_tabs()
    demo.launch()
